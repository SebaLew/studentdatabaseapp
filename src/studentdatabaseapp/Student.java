package studentdatabaseapp;

import java.util.Scanner;

public class Student {

    private String firstName;
    private String lastName;
    private int gradeYear;
    private String studentID;
    private String courses= "";
    private int tuitionBalance =0 ;
    private static int courseCost = 600;
    private static int id = 1000;

    //constructor: enter student name and year

    public Student() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter student's first name: ");
        this.firstName=in.nextLine();

        System.out.println("Enter student's last name: ");
        this.lastName=in.nextLine();

        System.out.println("1 - Freshmen \n2 - Sophomore \n3 - Junior \n4 - Senior \nEnter student's class level: ");
        this.gradeYear=in.nextInt();

        setStudentID();

    }


    //generate an ID
    private void setStudentID() {
        //grade level + static id
        id++;
        this.studentID = gradeYear + "" + id;
    }
    // enroll in courses

    public void enroll() {
        //get inside a loop user hits 0
        do {
            System.out.print("Enter course to enroll (Q to quit): ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();
            if (!course.equals("Q")) {
                courses = courses + "\n" + course;
                tuitionBalance = tuitionBalance + courseCost;
            } else {
                break;
            }
        } while (1 !=0 );
            System.out.println("ENROLLED IN: " + courses);
        }

    //view balance
    public void viewBalance() {
        System.out.println("Your balance is: $" + tuitionBalance);

    }

    //pay tuition
    public void payTuition () {
        System.out.println("Enter your payment: $");
        Scanner in = new Scanner(System.in);
        int payment = in.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your payment of $" + payment);
        viewBalance();
    }

    //show status
    public String showInfo() {
        return "Name: " + firstName + " " + lastName +
                "\nCourses enrolled:" + courses +
                "\nBalance: $" + tuitionBalance;
    }

}
